# adw-gtk3

Required by gnome-shell-extension-custom-accent-colors

The theme from libadwaita ported to GTK-3

https://github.com/lassekongo83/adw-gtk3

<br><br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/gnome/extensions/adw-gtk3.git
```
